# misc_PCGR_CPSR

Miscellaneous files related to setting up/running PCGR and CPSR in a singularity container in HPC with slurm load manager.

Contents of the repository:

- definition files to build singularity images containing [PCGR](https://github.com/sigven/pcgr) and [CPSR](https://github.com/sigven/cpsr) 
- wrappers that can be used to submit slurm jobs (for example into HPC in [TSD](https://www.uio.no/english/services/it/research/sensitive-data/about/index.html))


Various branches of the repository will be dedicated to various versions of PCGR and CPSR. Check for availability of the version you are looking for.

## Singularity Definition Files

Singularity build version:

Building singularity image:

```
VERSION=""
sudo singularity build \
	pcgr_cpsr_${VERSION}.sif \
	pcgr_cpsr_${VERSION}.def \
	&> pcgr_cpsr_${VERSION}.build.log
```

## Slurm Wrappers 

The provided wrappers are more of an usage example and do not necessairly pass all the possible arguments to PCGR/CPSR. Thus when considering which parameters 
should be set in PCGR/CPSR and whether it is better to hard wire them inside of the wrapper or to allow flexibility with passing them as input parameters to the wrapper, 
please consult [PCGR documentation](https://sigven.github.io/pcgr/articles/running.html) and [CPSR documentation](https://sigven.github.io/cpsr/articles/running.html).


### Execute pcgr for one sample:

```
ACCOUNT="<your_slurm_account>"
TIME="10:00:00"
MEM="8G"
STDOUT="<stdout_of_the_sbatch_job>"
STDERR="<stderr_of_the_sbatch_job>"

sbatch \
	--job-name "pcgr" \
	--account "${ACCOUNT}" \
	--time "${TIME}" \
	--mem-per-cpu "${MEM}" \
	--output "${STDOUT}" \
	--error "${STDERR}" \
	run_pcgr.sh \
		${SINGULARITY_IMAGE} \
		<other_input_version_specific_parameters>
```

Input parameters for `run_pcgr.sh`:

- `SINGULARITY_IMAGE`: absolute path to the singularity image containing PCGR and CPSR
- other input parameters are in detail described in the version branches

### Execute cpsr for one sample:


```
ACCOUNT="<your_slurm_account>"
TIME="10:00:00"
MEM="8G"
STDOUT="<stdout_of_the_sbatch_job>"
STDERR="<stderr_of_the_sbatch_job>"

sbatch \
	--job-name "cpsr" \
	--account "${ACCOUNT}" \
	--time "${TIME}" \
	--mem-per-cpu "${MEM}" \
	--output "${STDOUT}" \
	--error "${STDERR}" \
	run_cpsr.sh \
		${SINGULARITY_IMAGE} \
		<other_input_version_specific_parameters>


```

Input parameters for `run_cpsr.sh`:

- `SINGULARITY_IMAGE`: absolute path to the singularity image containing PCGR and CPSR
- other input parameters are in detail described in the version branches



### TSD Specifics

Singularity setup on the submit node before executing the wrappers:

```
module purge
module load singularity/3.7.3
```

Any files and paths used in sbatch commands have to be on `/cluster` partition.
